# Hint in spoilers

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2017-03-22 13:41.

## Version [1.0](https://gitlab.com/geocaching/hint-in-spoilers/milestones/1)

**Closed at 2017-03-22.**


### Issues
  * [[Feature 1]](https://gitlab.com/geocaching/hint-in-spoilers/issues/1) **Initial import** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

