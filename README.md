# To add hint in spoilers.

[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.gc/hint-in-spoilers.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.gc%22%20AND%20a%3A%22hint-in-spoilers%22)
![Build Status](https://gitlab.com/geocaching/hint-in-spoilers/badges/develop/build.svg)
[![The GNU General Public License, Version 3.0](https://img.shields.io/badge/license-GPL3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.txt)

See [documentation](https://geocaching.gitlab.io/hint-in-spoilers).

