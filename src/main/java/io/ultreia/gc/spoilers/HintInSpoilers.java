package io.ultreia.gc.spoilers;

/*-
 * #%L
 * Add hint in spoilers
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * To add hint inside the spoiler.
 * <p>
 * Created by tchemit on 21/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HintInSpoilers {

    public static void main(String[] args) throws Exception {

        String mappingPath;
        String outputPath;
        if (args.length == 0) {
            mappingPath = "/home/tchemit/projects/geocaching/smx_25032017/roadbook_v2/hint_mapping.csv";
            outputPath = "/home/tchemit/projects/geocaching/smx_25032017/dist/magic";
        } else {
            mappingPath = args[0];
            outputPath = args[1];
        }

        HintInSpoilersUI ui = new HintInSpoilersUI(mappingPath, outputPath);

        ui.show();

    }

}
