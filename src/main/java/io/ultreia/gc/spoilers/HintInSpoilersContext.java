package io.ultreia.gc.spoilers;

/*-
 * #%L
 * Add hint in spoilers
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;

/**
 * Created by tchemit on 21/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HintInSpoilersContext {

    private final Path outputPath;
    private final FontMetrics fontMetrics;
    private final Font font;
    private final JProgressBar progressBar;
    private final Color textColor;
    private final Color textBackgroundColor;

    public HintInSpoilersContext(Path outputPath, FontMetrics fontMetrics, Font font, JProgressBar progressBar, Color textColor, Color textBackgroundColor) {
        this.outputPath = outputPath;
        this.fontMetrics = fontMetrics;
        this.font = font;
        this.progressBar = progressBar;
        this.textColor = textColor;
        this.textBackgroundColor = textBackgroundColor;
    }

    public Path getOutputPath() {
        return outputPath;
    }

    public FontMetrics getFontMetrics() {
        return fontMetrics;
    }

    public Font getFont() {
        return font;
    }

    public JProgressBar getProgressBar() {
        return progressBar;
    }

    public Color getTextColor() {
        return textColor;
    }

    public Color getTextBackgroundColor() {
        return textBackgroundColor;
    }

    public void incrementsProgress() {

        if (progressBar!=null) {
            progressBar.setValue(progressBar.getValue() + 1);
        }
    }
}
