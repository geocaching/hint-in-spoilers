package io.ultreia.gc.spoilers;

/*-
 * #%L
 * Add hint in spoilers
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by tchemit on 21/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class HintInSpoilersEntry {

    /** Logger. */
    private static final Log log = LogFactory.getLog(HintInSpoilersEntry.class);

    private final URL spoilerPath;
    private String hint;

    HintInSpoilersEntry(URL spoilerPath, String hint) {
        this.spoilerPath = spoilerPath;
        this.hint = hint;
    }

    URL run(HintInSpoilersContext hintInSpoilersContext) {

        URL output;
        try {
            output = run0(hintInSpoilersContext);
        } catch (Exception e) {
            try {
                output = run0(hintInSpoilersContext);
            } catch (Exception e1) {
                throw new IllegalStateException("Can't treat spoiler: " + spoilerPath, e1);
            }
        }

        hintInSpoilersContext.incrementsProgress();

        log.info("end run for " + output);
        return output;
    }

    private URL run0(HintInSpoilersContext hintInSpoilersContext) throws IOException, InterruptedException {

        JDialog dialog = new JDialog();
        dialog.setLayout(new BorderLayout());

        ImageIcon icon;
        try {
            icon = new ImageIcon(ImageIO.read(spoilerPath));
        } catch (IOException e) {
            throw new IllegalStateException("Can't read spoiler from " + spoilerPath, e);
        }

        JLabel image = new JLabel(icon);

        String[] split = spoilerPath.toExternalForm().split("/");
        String filename = split[split.length - 1];

        log.debug("start run for " + filename);
        log.debug("text color: " + hintInSpoilersContext.getTextColor());
        log.debug("back color: " + hintInSpoilersContext.getTextBackgroundColor());
        dialog.add(image, BorderLayout.CENTER);

        JLabel label = new JLabel();
        label.setFont(hintInSpoilersContext.getFont());
        label.setBackground(hintInSpoilersContext.getTextBackgroundColor());
        label.setOpaque(true);
        label.setForeground(hintInSpoilersContext.getTextColor());

        dialog.add(label, BorderLayout.SOUTH);
        dialog.pack();

        int hintSize = hintInSpoilersContext.getFontMetrics().stringWidth(hint);
        int width = image.getWidth();

        if (width < 10) {
            Thread.sleep(100);

            width = image.getWidth();
        }
        if (hintSize > width) {
            log.debug(String.format("Need to compute hint for %s : %s", filename, hint));
            hint = computeHint(hint, width, hintInSpoilersContext.getFontMetrics());
        }

        label.setText(hint);
        dialog.pack();

        log.debug(String.format("Cache %s [width: %d] [hint size: %d] -- %s", filename, width, hintSize, hint));


        Container c = dialog.getContentPane();
        BufferedImage im = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_RGB);
        c.paint(im.getGraphics());
        Path output = hintInSpoilersContext.getOutputPath().resolve(filename);
        if (Files.exists(output)) {
            Files.delete(output);
        }
        ImageIO.write(im, "JPG", output.toFile());

        dialog.dispose();

        Runtime.getRuntime().gc();
        return output.toFile().toURI().toURL();
    }

    private String computeHint(String hint, int width, FontMetrics fontMetrics) {
        String[] split = hint.split(" ");
        String result = "<html>";
        String current = "";
        for (String s : split) {
            String newCurrent = current + " " + s;
            newCurrent = newCurrent.trim();
            int size = fontMetrics.stringWidth(current);
            int newSize = fontMetrics.stringWidth(newCurrent);
            if (size < width && newSize > width) {
                if (!result.equals("<html>")) {
                    result += "<br/>";
                }
                result += current.trim();
                current = s;
            } else {
                current = newCurrent.trim();
            }
        }
        if (!result.equals("<html>")) {
            result += "<br/>";
        }
        result += current.trim();
        return result + "</html>";
    }

}
