package io.ultreia.gc.spoilers;

/*-
 * #%L
 * Add hint in spoilers
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by tchemit on 21/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class HintInSpoilersModel {

    private Path mappingPath;
    private final Path tempPath;
    private Path outputPath;
    private Color textColor = Color.RED;
    private Color backgroundColor = Color.WHITE;
    private String fontName = "monospace";
    private int fontSize = 24;
    private final URL exampleUrl;
    private final List<HintInSpoilersEntry> spoilers = new LinkedList<>();

    public HintInSpoilersModel() {
        exampleUrl = getClass().getClassLoader().getResource("example.jpg");
        Objects.requireNonNull(exampleUrl);
        try {
            tempPath = Files.createTempDirectory("hint-in-spoilers");
        } catch (IOException e) {
            throw new IllegalStateException("Can't create temparary path", e);
        }
    }

    public Path getTempPath() {
        return tempPath;
    }

    public URL getExampleUrl() {
        return exampleUrl;
    }

    public Path getMappingPath() {
        return mappingPath;
    }

    public void setMappingPath(Path mappingPath) {
        this.mappingPath = mappingPath;
        if (mappingPath != null && Files.exists(mappingPath)) {
            loadSpoilers();
        }
    }

    public Path getOutputPath() {
        return outputPath;
    }

    public void setOutputPath(Path outputPath) {
        this.outputPath = outputPath;
        if (!Files.exists(this.outputPath)) {
            try {
                Files.createDirectories(this.outputPath);
            } catch (IOException e) {
                throw new IllegalStateException("Can't create output dir: " + outputPath, e);
            }
        }
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public Color getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Color textBackgroundColor) {
        this.backgroundColor = textBackgroundColor;
    }

    public int getFontSize() {
        return fontSize;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public List<HintInSpoilersEntry> getSpoilers() {
        return spoilers;
    }

    public boolean isValid() {
        return outputPath != null && mappingPath != null && Files.exists(mappingPath);
    }

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    private void loadSpoilers() {

        spoilers.clear();
        try (BufferedReader reader = Files.newBufferedReader(mappingPath)) {

            String line;

            while ((line = reader.readLine()) != null) {

                String[] split = line.split(":");
                String s = split[0];
                if (!s.contains("://")) {
                    s = "file://" + s;
                }
                URL spoilerPath = new URL(s);
                String hint = split[1];

                HintInSpoilersEntry spoiler = new HintInSpoilersEntry(spoilerPath, hint);
                spoilers.add(spoiler);
            }

        } catch (IOException e) {
            throw new RuntimeException("Can't read mapping file");
        }
    }

    public Font getFont() {
        return new Font(getFontName(), Font.PLAIN, getFontSize());
    }
}
