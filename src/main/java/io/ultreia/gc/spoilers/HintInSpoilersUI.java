package io.ultreia.gc.spoilers;

/*-
 * #%L
 * Add hint in spoilers
 * %%
 * Copyright (C) 2017 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.jaxx.runtime.swing.SwingUtil;
import org.nuiton.jaxx.widgets.file.JaxxFileChooser;
import org.nuiton.util.StringUtil;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.concurrent.Executors;

/**
 * Created by tchemit on 21/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class HintInSpoilersUI {

    /** Logger. */
    private static final Log log = LogFactory.getLog(HintInSpoilersUI.class);

    private final HintInSpoilersModel model;

    private final JFrame frame;
    private final JButton apply;
    private final JLabel mappingLabel;
    private final JLabel outputLabel;
    private final JProgressBar progressBar;
    private final JLabel exampleLabel;

    HintInSpoilersUI(String mappingPath, String outputPath) {
        frame = new JFrame("Add hint in spoilers");
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setLayout(new BorderLayout());

        JPanel configuration = new JPanel();
        configuration.setLayout(new GridLayout(0, 1));
        frame.add(configuration, BorderLayout.WEST);

        progressBar = new JProgressBar();
        progressBar.setBorderPainted(true);
        progressBar.setStringPainted(true);

        JPanel mappingPanel = new JPanel();
        mappingPanel.setLayout(new BorderLayout());
        configuration.add(mappingPanel);
        mappingLabel = new JLabel("Mapping path");
        mappingPanel.add(mappingLabel, BorderLayout.WEST);

        // Mapping file configuration

        JButton mappingAction = new JButton();
        mappingAction.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Optional<Path> path = Optional.ofNullable(JaxxFileChooser.forLoadingFile().setApprovalText("Ok").setTitle("Select mapping file").choose()).map(File::toPath);
                if (path.isPresent()) {
                    model.setMappingPath(path.get());
                    updateApply();
                }
            }
        });
        mappingAction.setToolTipText("Choose the mapping containing location of spoilers and hint to apply.");
        mappingAction.setIcon(SwingUtil.createActionIcon("open"));
        mappingPanel.add(mappingAction, BorderLayout.EAST);

        // Output file configuration

        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BorderLayout());
        configuration.add(outputPanel);
        outputLabel = new JLabel("Output path");
        outputPanel.add(outputLabel, BorderLayout.WEST);
        JButton outputAction = new JButton();
        outputAction.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {

                Optional<Path> path = Optional.ofNullable(JaxxFileChooser.forLoadingDirectory().setApprovalText("Ok").setTitle("Choose output directory").choose()).map(File::toPath);
                if (path.isPresent()) {
                    model.setOutputPath(path.get());
                    updateApply();
                }

            }
        });
        outputAction.setIcon(SwingUtil.createActionIcon("open"));
        outputAction.setToolTipText("Choose output directory <where to generate spoilers with hint.");
        outputPanel.add(outputAction, BorderLayout.EAST);

        // Font size configuration

        JPanel fontSizePanel = new JPanel();
        fontSizePanel.setLayout(new BorderLayout());
        configuration.add(fontSizePanel);
        JLabel fontSizeLabel = new JLabel("Font size");
        fontSizePanel.add(fontSizeLabel, BorderLayout.WEST);
        JSpinner fontSizeEditor = new JSpinner();
        fontSizeEditor.setModel(new SpinnerNumberModel(24, 12, 50, 1));
        fontSizePanel.add(fontSizeEditor, BorderLayout.EAST);
        fontSizeEditor.getModel().addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                SpinnerNumberModel source = (SpinnerNumberModel) e.getSource();
                int value = (int) source.getValue();
                model.setFontSize(value);
                SwingUtilities.invokeLater(HintInSpoilersUI.this::updateExample);
            }
        });

        // Hint text color

        JPanel hintTextColorPanel = new JPanel();
        hintTextColorPanel.setLayout(new BorderLayout());
        configuration.add(hintTextColorPanel);
        JLabel hintTextColorLabel = new JLabel("Hint text color");
        hintTextColorPanel.add(hintTextColorLabel, BorderLayout.WEST);
        JButton hintTextColorAction = new JButton();
        hintTextColorAction.setForeground(Color.RED);
        hintTextColorAction.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {

                JColorChooser colorChooser = new JColorChooser(Color.RED);
                JDialog dialog = JColorChooser.createDialog((JButton) e.getSource(),
                        "Pick hint text color",
                        true,  //modal
                        colorChooser,
                        e1 -> {
                            Color selectedColor = colorChooser.getSelectionModel().getSelectedColor();
                            model.setTextColor(selectedColor);
                            hintTextColorAction.setForeground(selectedColor);
                            SwingUtilities.invokeLater(HintInSpoilersUI.this::updateExample);
                        },  //OK button handler
                        null);
                dialog.setVisible(true);
                dialog.dispose();

            }
        });
        hintTextColorAction.setText("Pick");
        hintTextColorAction.setToolTipText("Pick hint text color.");
        hintTextColorPanel.add(hintTextColorAction, BorderLayout.EAST);


        // Hint background color

        JPanel hintBackgroundColorPanel = new JPanel();
        hintBackgroundColorPanel.setLayout(new BorderLayout());
        configuration.add(hintBackgroundColorPanel);
        JLabel hintBackgroundColorLabel = new JLabel("Hint background color");

        hintBackgroundColorPanel.add(hintBackgroundColorLabel, BorderLayout.WEST);
        JButton hintBackgroundColorAction = new JButton();
        hintBackgroundColorAction.setForeground(Color.WHITE);
        hintBackgroundColorAction.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {

                JColorChooser colorChooser = new JColorChooser(Color.WHITE);
                JDialog dialog = JColorChooser.createDialog((JButton) e.getSource(),
                        "Pick hint background color",
                        true,  //modal
                        colorChooser,
                        e1 -> {
                            Color selectedColor = colorChooser.getSelectionModel().getSelectedColor();
                            model.setBackgroundColor(selectedColor);
                            hintBackgroundColorAction.setForeground(selectedColor);
                            SwingUtilities.invokeLater(HintInSpoilersUI.this::updateExample);
                        },  //OK button handler
                        null);
                dialog.setVisible(true);
                dialog.dispose();

            }
        });
        hintBackgroundColorAction.setText("Pick");
        hintBackgroundColorAction.setToolTipText("Pick hint background color.");
        hintBackgroundColorPanel.add(hintBackgroundColorAction, BorderLayout.EAST);

        // Example panel

        JScrollPane exampleScrollPane = new JScrollPane();
        exampleScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        frame.add(exampleScrollPane, BorderLayout.EAST);
        exampleLabel = new JLabel();
        exampleScrollPane.setViewportView(exampleLabel);

        JPanel actions = new JPanel();
        actions.setLayout(new GridLayout(1, 0));
        frame.add(actions, BorderLayout.SOUTH);

        apply = new JButton();
        apply.setAction(new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent e) {

                progressBar.setMinimum(0);
                progressBar.setMaximum(model.getSpoilers().size());

                Executors.newSingleThreadExecutor().submit(HintInSpoilersUI.this::run);

            }
        });
        apply.setText("Apply");
        actions.add(apply);

        frame.pack();

        model = new HintInSpoilersModel();
        model.setMappingPath(Paths.get(mappingPath));
        model.setOutputPath(Paths.get(outputPath));

        updateApply();
        updateExample();
        frame.pack();
    }

    private void updateApply() {
        Path outputPath = model.getOutputPath();
        outputLabel.setText("Output path: " + outputPath);
        Path mappingPath = model.getMappingPath();
        if (mappingPath != null && Files.exists(mappingPath)) {
            mappingLabel.setText(String.format("Mapping path: %s (%d spoiler(s))", mappingPath, model.getSpoilers().size()));
        } else {
            mappingLabel.setText("Mapping path: <Not selected>");
        }

        apply.setEnabled(model.isValid());
        frame.pack();
    }

    private void updateExample() {

        HintInSpoilersEntry entry = new HintInSpoilersEntry(model.getExampleUrl(), "This is an hint");

        URL output = entry.run(createHintInSpoilersContext(model.getTempPath(), null));

        log.debug("reload example: " + output);
        ImageIcon icon;
        try {
            icon = new ImageIcon(ImageIO.read(output));
        } catch (IOException e) {
            throw new IllegalStateException("Can't read example image from " + output, e);
        }

        exampleLabel.setIcon(icon);
        int iconHeight = icon.getIconHeight();
        int iconWidth = icon.getIconWidth();
        log.debug(String.format("Image size [%d,%d]", iconWidth, iconHeight));

    }

    void show() {
        frame.setModalExclusionType(Dialog.ModalExclusionType.NO_EXCLUDE);
        frame.setVisible(true);
    }

    private void run() {

        log.info("Reading mapping file: " + model.getMappingPath());
        log.info("Output dir: " + model.getOutputPath());

        long t0 = System.nanoTime();

        java.util.List<HintInSpoilersEntry> spoilers = model.getSpoilers();

        log.info("Found " + spoilers.size() + " spoiler(s) to treat.");

        JDialog progressionDialog = new JDialog(frame, true);
        progressionDialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        progressionDialog.setLayout(new BorderLayout());
        progressionDialog.add(progressBar, BorderLayout.CENTER);
        progressionDialog.setSize(new Dimension(frame.getWidth() - 10, frame.getHeight() - 10));
        progressionDialog.setUndecorated(true);

        SwingUtil.center(frame, progressionDialog);
        SwingUtilities.invokeLater(() -> progressionDialog.setVisible(true));

        try {
            HintInSpoilersContext context = createHintInSpoilersContext(model.getOutputPath(), progressBar);

            spoilers.parallelStream().forEach(s -> s.run(context));

        } finally {

            progressionDialog.setVisible(false);
        }


        long t1 = System.nanoTime();

        log.info("Done in " + StringUtil.convertTime(t1 - t0));

    }

    private HintInSpoilersContext createHintInSpoilersContext(Path outputPath, JProgressBar progressBar) {
        Font font = model.getFont();
        FontMetrics fontMetrics = frame.getGraphics().getFontMetrics(font);
        return new HintInSpoilersContext(outputPath, fontMetrics, font, progressBar, model.getTextColor(), model.getBackgroundColor());
    }

}
